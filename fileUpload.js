
const axios = require('axios');
const formdata = require('form-data');

const uploadFile = async function(stream, fileName, contentType){
    
    var form = new formdata();
    form.append('file', stream, {filename: fileName, contentType: contentType} );
    form.append('ttl', "30 mins");
    
    console.log("stream: "+stream);
    var response = "";
    var form_headers = form.getHeaders();
    
    try{
        console.log("posting data");
        response = await axios.post('https://preproduction-persist.signzy.tech/api/files/upload', form, {headers: form_headers})
        response = await response.data;
    }
    catch(err)
    {
        response = err;
    }
    
    return response;

}

module.exports = uploadFile;