const fm = require('fastify-multipart');
const uploadFile = require('./fileUpload.js');
const panExtraction = require('./panExtraction.js');
const getIdentity = require('./createIdentity.js');
const readFile = require('./fileRead.js');

async function routes(childRoute)
{
    childRoute.register(fm);
    childRoute.post('/extractPan',async(req,res)=>{
        var data = await req.file();
        
        var credentials = await readFile;

        var fileinfo = await uploadFile(data.file,data.filename,data.mimetype);
        if(fileinfo.file)
        {

        let identify = await getIdentity(credentials.id, credentials.userId, fileinfo.file.directURL);
        if(identify?.data?.id)
        {
            var panData = await panExtraction(identify.data.id, identify.data.accessToken);
            return panData;
        }
        else
            {res.statusCode = 500;
                return{
                error:{
                    message: "Identity cannot be created properly"
                }
            }
            }
        }
        else
        {
            res.statusCode = 500;
            return{
                error:{
                    message: "File cannot be properly uploaded"
                }
            }
        }

        
    })
}

module.exports = routes;
