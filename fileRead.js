const fs = require('fs');

const readFile = new Promise((resolve,reject)=>{
    fs.readFile(__dirname+'/credentials.json',(err,docs)=>{
        if(err)
            reject(err);
        else
            resolve(JSON.parse(docs));
    })
})


module.exports = readFile;