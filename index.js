const fastify = require('fastify');
const childroutes = require('./routes');

const app = fastify();

app.register(childroutes);




app.listen(3000, (err,address)=>{
    if(err)
    {
        console.log(err);
        process.exit(1);
    }
    console.log(`server listening on ${address}`);
})