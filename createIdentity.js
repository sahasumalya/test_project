const axios = require('axios');
const getIdentity = async function(token, userid, imageUrl)
{
    var identityData = {};
    let body = {
        "type": "individualPan",
        "email": "sumalya.saha@signzy.com",
        "callbackUrl": "https://www.youtube.com/",
        "images": [
            imageUrl
        ]
    }
    try
    {
        var rawIdentity = await axios.post('https://preproduction.signzy.tech/api/v2/patrons/'+userid+'/identities', body, {
            headers : {'Content-Type': 'application/json',
                    'Authorization': token}
           
        })
        identityData.data = rawIdentity.data;
    }
    catch(err)
    {
        identityData.error = err;
    }
   return identityData;
}

module.exports = getIdentity;