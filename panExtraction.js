const axios = require('axios');
const panExtraction = async function(itemid, accessToken)
{

    var panDetails = {};
    var body =  {
        "service":"Identity",
        "itemId":itemid,
        "task":"autoRecognition",
        "accessToken":accessToken,
        "essentials":{}
      }
    try{
        console.log("trying to fetch details");
        let rawPanData = await axios.post('https://preproduction.signzy.tech/api/v2/snoops', body, {
            headers : {'Content-Type': 'application/json'}
        })
        console.log(rawPanData);
        panDetails.data = rawPanData.data;
    }
    catch(err)
    {
        panDetails.error = err;
    }

    return panDetails;
    

}

module.exports = panExtraction;